import React, {Component} from "react";
import AppHeader from "../MakeHeader/MakeHeader";
import SearchPanel from "../SearchPanel/SearchPanel";
import TodoList from "../todo-list/todo-list";
import ItemStatusFilter from "../item-status-filter/";
import "../custom.css";
import Additem from "../add-item/add-item";


export default class App extends Component{

    maxId = 100;

    state = {
        todoData : [
            this.createTodoItem('Выпить кофейка'),
            this.createTodoItem('ake Awesome App'),
            this.createTodoItem('Послушать трек скрипа')
        ],
        term : '',
        filter: 'all'
    };

    onSearchChange = (term) =>{
      this.setState({term}
      );
    };

    onFilterChange = (filter) => {
        this.setState({filter})
    };

    createTodoItem(label) {
        return{
            label,
            important: false,
            done: false,
            id: this.maxId++
        }
    };

    deleteItem = (id) => {
     this.setState(({ todoData }) =>{
         const idx = todoData.findIndex((el)=> el.id === id);
         const newArray = [
             ...todoData.slice(0, idx),
             ...todoData.slice(idx + 1)
         ];


         return{
             todoData: newArray
         };
     });
    };

    addItem = (text) => {
       const newItem = this.createTodoItem(text);

       this.setState(({todoData}) =>{
           const newArr = [
               ...todoData,
               newItem
           ];
           return{
               todoData: newArr
           };
           });

    };

    toggleProperty(arr, id, propName){
        const idx = arr.findIndex((el)=> el.id === id);
        const oldItem = arr[idx];
        const newItem = {...oldItem, [propName]: !oldItem[propName]};
        return [
            ...arr.slice(0, idx),
            newItem,
            ...arr.slice(idx + 1)
        ];
    };

    onToggleDone = (id) => {
        this.setState(({ todoData }) => {
            return{
           todoData: this.toggleProperty(todoData, id, 'done')
            };
        });
    };

    onToggleImportant = (id) =>{
        this.setState(({ todoData }) => {
            return{
            todoData: this.toggleProperty(todoData, id, 'important')
            };
        });
    };

    search(items, term) {
        if (term.length === 0) {
            return items;
        };
     return items.filter((item) => {
          return item.label.toLowerCase().indexOf(term.toLowerCase()) > -1;
      });
      };

    filter(items, filter){
        switch (filter) {
            case 'all':
                return items;
            case 'active':
                return items.filter((item) => !item.done);
            case 'done':
                return items.filter((item) => item.done);
            default:
               return items;
        }
    }

    render() {
        const { todoData, term, filter } = this.state;
        const visibleItems = this.filter(this.search(todoData, term), filter);
        const doneCount = todoData.filter((el) => el.done).length;
        const todoCount = todoData.length - doneCount;
        return(
            <div>
                <AppHeader toDo={todoCount} done={doneCount} />
                <div className="input-wrapper">
                    <SearchPanel onSearchChange={this.onSearchChange}/>
                    <ItemStatusFilter filter={filter}
                     onFilterChange={this.onFilterChange}   />
                </div>
                <TodoList
                    todos={visibleItems}
                    onDeleted={ this.deleteItem }
                    onToggleImportant = {this.onToggleImportant}
                    onToggleDone={this.onToggleDone}
            />
                <Additem onItemAdded={ this.addItem } />
            </div>
        );
    }

}
