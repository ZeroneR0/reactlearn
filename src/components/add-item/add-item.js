import React, {Component} from "react";
import './add-item.css';

export default class Additem extends Component{

    state = {
        label: ''
    };

    onLabelChange = (e) => {
        this.setState({label: e.target.value});
    };

    onSubmit = (e) =>{
        e.preventDefault();
        this.props.onItemAdded(this.state.label);
        this.setState({
            label:''
        });
    };

    render() {
        return(
            <form className="item-add-form d-flex"
            onSubmit={this.onSubmit}>
                <input type="text" className="form-control" onChange={this.onLabelChange}
                placeholder="what needs to be done"
                    value={this.state.label} />
                <button type="submit" className="btn btn-primary">
                    Добавить задание
                </button>
            </form>
        )
    }
}

