import React, {Component} from "react";
import './todo-list-item.css';

class TodoListItem extends Component {



    render() {
        const { label, onDeleted, onToggleDone, onToggleImportant, important, done } = this.props;

        let classNames = 'todo-list-item';
        if (done) {
            classNames += ' done';
        }

        if (important) {
            classNames += ' important';
        }


        return(
            <span className={classNames}>
                <span
                  className ="todo-list-item-label"
                  onClick={onToggleDone}>
                 {label}
                 </span>
            <div className="button-wrapper">
                        <button type="button" className="btn btn-danger" onClick={onDeleted}>Del</button>
                        <button type="button" className="btn btn-success" onClick={onToggleImportant}>!</button>
            </div>
             </span>
        )
    };
}


export default TodoListItem;